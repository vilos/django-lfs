What is it?
===========

LFS is an online shop based on Python, Django and jQuery.

Information
===========

For more information please visit:

* http://www.getlfs.com

Documentation
=============

For the latest documentation please visit:

* http://packages.python.org/django-lfs/

Please note that this is work in progress and will be much more in near
future.

Demo shop
=========

A demo shop can be tried here:

* http://demo.getlfs.com

Credits
=======

* Dirk Kommol (Demmelhuber Holz & Raum - http://www.demmelhuber.net)

  Power user, smart adviser, sponsor, products for demo shop and much more

* Reza Muhammad (rezmuh)
  
  Reporting bugs

* Michael Thornhill
  
  Integration of PayPal (django-paypal), fixing bugs,

* Martin Mahner

  Reporting bugs, providing patches

* Icons are from Mark James' Silk icon set 1.3

  (http://www.famfamfam.com/lab/icons/silk/)

* The star rating is based on CSS Star Rating Redux

  (http://komodomedia.com/blog/index.php/2007/01/20/css-star-rating-redux/)

Changes
=======

0.2.3  (2009-12-28)
-------------------

* Display the parent product ("product with variants") of a variant within 
  category products instead of the default variant (Michaela Hering)

* Added meta title to product's and Category's SEO tab.

* Improved vouchers

0.2.2  (2009-10-22)
-------------------

* Reverted removed voucher taxes.

0.2.1  (2009-10-22)
-------------------

* Solved issue #1: "postgresql syncdb problem" (Michael Thornhill)

* Made all tests passing for PostgreSQL (Michael Thornhill)

* Take official release of django-paypal (Michael Thornhill)

* Improved vouchers management

* Added more documentation

0.2.0  (2009-10-18)
-------------------

* Added vouchers

* Added price / tax methods to Cart model

* Cleaned up documentation

* Added first how-tos to documentation

0.1.4  (2009-10-14)
-------------------

* Added 'Development Status :: 3 - Alpha' to setup.py

* Added more dependencies to setup.py

0.1.3  (2009-10-12)
-------------------

* Added static folder to release

0.1.2  (2009-10-12)
-------------------

* Fixed typo in dependencies

0.1.1  (2009-10-12)
-------------------

* Added dependencies to setup.py

0.1  (2009-10-11)
-------------------

* Initial public release