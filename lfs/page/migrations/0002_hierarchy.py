
from south.db import db
from django.db import models
from lfs.page.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding field 'Page.parent'
        db.add_column('page_page', 'parent', orm['page.page:parent'])
        
        # Adding field 'Page.level'
        db.add_column('page_page', 'level', orm['page.page:level'])
        
        # Adding field 'Page.image'
        db.add_column('page_page', 'image', orm['page.page:image'])
        
        # Adding field 'Page.exclude_from_navigation'
        db.add_column('page_page', 'exclude_from_navigation', orm['page.page:exclude_from_navigation'])
        
    
    
    def backwards(self, orm):
        
        # Deleting field 'Page.parent'
        db.delete_column('page_page', 'parent_id')
        
        # Deleting field 'Page.level'
        db.delete_column('page_page', 'level')
        
        # Deleting field 'Page.image'
        db.delete_column('page_page', 'image')
        
        # Deleting field 'Page.exclude_from_navigation'
        db.delete_column('page_page', 'exclude_from_navigation')
        
    
    
    models = {
        'page.page': {
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'exclude_from_navigation': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'children'", 'blank': 'True', 'null': 'True', 'to': "orm['page.Page']"}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '999'}),
            'short_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }
    
    complete_apps = ['page']
