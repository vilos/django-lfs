
from south.db import db
from django.db import models
from lfs.page.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding model 'Page'
        db.create_table('page_page', (
            ('id', orm['page.Page:id']),
            ('title', orm['page.Page:title']),
            ('slug', orm['page.Page:slug']),
            ('short_text', orm['page.Page:short_text']),
            ('body', orm['page.Page:body']),
            ('active', orm['page.Page:active']),
            ('position', orm['page.Page:position']),
            ('file', orm['page.Page:file']),
        ))
        db.send_create_signal('page', ['Page'])
        
    
    
    def backwards(self, orm):
        
        # Deleting model 'Page'
        db.delete_table('page_page')
        
    
    
    models = {
        'page.page': {
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '999'}),
            'short_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }
    
    complete_apps = ['page']
