
from south.db import db
from django.db import models
from lfs.portlet.models import *

class Migration:
    
    def forwards(self, orm):
        
        # Adding model 'FilterPortlet'
        db.create_table('portlet_filterportlet', (
            ('id', orm['portlet.FilterPortlet:id']),
            ('title', orm['portlet.FilterPortlet:title']),
            ('show_product_filters', orm['portlet.FilterPortlet:show_product_filters']),
            ('show_price_filters', orm['portlet.FilterPortlet:show_price_filters']),
        ))
        db.send_create_signal('portlet', ['FilterPortlet'])
        
        # Adding model 'CartPortlet'
        db.create_table('portlet_cartportlet', (
            ('id', orm['portlet.CartPortlet:id']),
            ('title', orm['portlet.CartPortlet:title']),
        ))
        db.send_create_signal('portlet', ['CartPortlet'])
        
        # Adding model 'DeliveryTimePortlet'
        db.create_table('portlet_deliverytimeportlet', (
            ('id', orm['portlet.DeliveryTimePortlet:id']),
            ('title', orm['portlet.DeliveryTimePortlet:title']),
        ))
        db.send_create_signal('portlet', ['DeliveryTimePortlet'])
        
        # Adding model 'TextPortlet'
        db.create_table('portlet_textportlet', (
            ('id', orm['portlet.TextPortlet:id']),
            ('title', orm['portlet.TextPortlet:title']),
            ('text', orm['portlet.TextPortlet:text']),
        ))
        db.send_create_signal('portlet', ['TextPortlet'])
        
        # Adding model 'TopsellerPortlet'
        db.create_table('portlet_topsellerportlet', (
            ('id', orm['portlet.TopsellerPortlet:id']),
            ('title', orm['portlet.TopsellerPortlet:title']),
            ('limit', orm['portlet.TopsellerPortlet:limit']),
        ))
        db.send_create_signal('portlet', ['TopsellerPortlet'])
        
        # Adding model 'AverageRatingPortlet'
        db.create_table('portlet_averageratingportlet', (
            ('id', orm['portlet.AverageRatingPortlet:id']),
            ('title', orm['portlet.AverageRatingPortlet:title']),
        ))
        db.send_create_signal('portlet', ['AverageRatingPortlet'])
        
        # Adding model 'RelatedProductsPortlet'
        db.create_table('portlet_relatedproductsportlet', (
            ('id', orm['portlet.RelatedProductsPortlet:id']),
            ('title', orm['portlet.RelatedProductsPortlet:title']),
        ))
        db.send_create_signal('portlet', ['RelatedProductsPortlet'])
        
        # Adding model 'RecentProductsPortlet'
        db.create_table('portlet_recentproductsportlet', (
            ('id', orm['portlet.RecentProductsPortlet:id']),
            ('title', orm['portlet.RecentProductsPortlet:title']),
        ))
        db.send_create_signal('portlet', ['RecentProductsPortlet'])
        
        # Adding model 'PagesPortlet'
        db.create_table('portlet_pagesportlet', (
            ('id', orm['portlet.PagesPortlet:id']),
            ('title', orm['portlet.PagesPortlet:title']),
        ))
        db.send_create_signal('portlet', ['PagesPortlet'])
        
        # Adding model 'CategoriesPortlet'
        db.create_table('portlet_categoriesportlet', (
            ('id', orm['portlet.CategoriesPortlet:id']),
            ('title', orm['portlet.CategoriesPortlet:title']),
            ('start_level', orm['portlet.CategoriesPortlet:start_level']),
            ('expand_level', orm['portlet.CategoriesPortlet:expand_level']),
        ))
        db.send_create_signal('portlet', ['CategoriesPortlet'])
        
    
    
    def backwards(self, orm):
        
        # Deleting model 'FilterPortlet'
        db.delete_table('portlet_filterportlet')
        
        # Deleting model 'CartPortlet'
        db.delete_table('portlet_cartportlet')
        
        # Deleting model 'DeliveryTimePortlet'
        db.delete_table('portlet_deliverytimeportlet')
        
        # Deleting model 'TextPortlet'
        db.delete_table('portlet_textportlet')
        
        # Deleting model 'TopsellerPortlet'
        db.delete_table('portlet_topsellerportlet')
        
        # Deleting model 'AverageRatingPortlet'
        db.delete_table('portlet_averageratingportlet')
        
        # Deleting model 'RelatedProductsPortlet'
        db.delete_table('portlet_relatedproductsportlet')
        
        # Deleting model 'RecentProductsPortlet'
        db.delete_table('portlet_recentproductsportlet')
        
        # Deleting model 'PagesPortlet'
        db.delete_table('portlet_pagesportlet')
        
        # Deleting model 'CategoriesPortlet'
        db.delete_table('portlet_categoriesportlet')
        
    
    
    models = {
        'portlet.averageratingportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.cartportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.categoriesportlet': {
            'expand_level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.deliverytimeportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.filterportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'show_price_filters': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'show_product_filters': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.pagesportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.recentproductsportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.relatedproductsportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.textportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'portlet.topsellerportlet': {
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }
    
    complete_apps = ['portlet']
