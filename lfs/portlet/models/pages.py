# django imports
from django import forms
from django.db import models
from django.core.cache import cache
from django.template import RequestContext
from django.template.loader import render_to_string

# portlets imports
from portlets.models import Portlet
from portlets.utils import register_portlet

# lfs imports
from lfs.page.models import Page
from lfs.core.utils import get_current_pages, PageTree

class PagesPortlet(Portlet):
    """A portlet to display pages.
    """
    start_level = models.PositiveSmallIntegerField(default=1)
    expand_level = models.PositiveSmallIntegerField(default=1)
        
    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        
        request = context.get("request")
        object = context.get("page")
        
        current_pages = get_current_pages(request, object)

        pt = PageTree(current_pages, self.start_level, self.expand_level)
        page_tree = pt.get_page_tree()

        return render_to_string("lfs/portlets/pages.html", RequestContext(request, {
            "title" : self.title,
            "pages" : page_tree,
            "MEDIA_URL" : context.get("MEDIA_URL"),
            "page" : object,
        }))

    def form(self, **kwargs):
        """
        """
        return PagesForm(instance=self, **kwargs)

class PagesForm(forms.ModelForm):
    """
    """
    class Meta:
        model = PagesPortlet
