# django imports
from django.contrib.auth.decorators import permission_required
from django.template import RequestContext
from django.template.loader import render_to_string
from django.core.cache import cache

# lfs imports
from lfs.page.models import Page

@permission_required("manage_shop", login_url="/login/")
def manage_pages_portlet(request, page_id,
    template_name="manage/page/manage_pages_portlet.html"):
    """Returns a management portlet of all pages.
    """
    cache_key = "manage-page-portlet"
    result = cache.get(cache_key)
    if result is not None:
        return result

    pages = []
    for page in Page.objects.filter(parent = None):
        children = pages_portlet_children(request, page)
        pages.append({
            "id" : page.id,
            "slug" : page.slug,
            "title" : page.title,
            "url"  : page.get_absolute_url(),            
            "children" : children,
            "is_current" : _is_current_page(request, page),
        })

    result = render_to_string(template_name, RequestContext(request, {
        "pages" : pages,
        "page_id" : page_id,
    }))

    cache.set(cache_key, result)
    return result

@permission_required("manage_shop", login_url="/login/")
def pages_portlet_children(request, page):
    """Returns the children of the given page as HTML.
    """
    pages = []
    for child_page in page.children.all():        
        children = pages_portlet_children(request, child_page)
        pages.append({
            "id" : child_page.id,
            "slug" : child_page.slug,
            "title" : child_page.title,
            "url"  : child_page.get_absolute_url(),
            "children" : children,
            "is_current" : _is_current_page(request, child_page),            
        })
    
    result = render_to_string("manage/page/manage_pages_portlet_children.html", RequestContext(request, {
        "page" : page,
        "pages" : pages
    }))
    
    return result
        
def _is_current_page(request, page):
    """Returns True if the passed page is the current page.
    """
    id = request.path.split("/")[-1]
    return str(page.id) == id