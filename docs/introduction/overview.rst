.. index:: Overview

========
Overview
========

Goal
====

LFS aims to be easy-to-use and fast for all: shop owners, shop customers and 
developers.

Features
=========

For an up-to-date list of features please visit the official website: 
http://www.getlfs.com/features.