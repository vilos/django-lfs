.. index:: Page, HTML

====
Page
====

Overview
========

* A page is a simple HTML page within the shop.
* A page can be used to display information like terms and conditions or
  to create landing pages.
* Pages are automatically displayed within the pages portlet.

Add a page
==========

To add a page proceed as following:

1. Go to Management / HTML / Pages.
2. Click on ``Add page``. Please note: if there are no Static blocks yet
   at all you will redirected automatically to the add form.
3. Fill in the form and click on ``Add page``

Remove a page
=============

To remove a page proceed as following:

1. Browse to the page you want to delete.
2. Click on the ``Delete page`` button and answer the confirmation question
   with yes.

Data fields
===========

Title
    The title of the page. This is displayed on top of the page as well as
    within the meta title tag.

Slug
    The unique last part of the URL to the page.

Short text
    The short text of the page. This is displayed within overviews.

Text
    The main text of the page.

Active
    If this is checked the page is active. Only active pages are displayed to
    the shop users.

Position
    Pages are ordered by position. Lower numbers come first.

.. index:: pair: File; Downloadable

File
    A file which can be uploaded. If a file has been uploaded a download link
    is automatically displayed at the bottom of the page.
