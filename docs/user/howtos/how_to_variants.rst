How to add a product with variants
==================================

In this how-to you will learn how to add a product with variants.

1. Go to the shop's management interface

2. Go to Catalog / Products

3. Click on *"Add product"* in order to add new product
4. Enter the *"Name", the "slug", the "SKU" and the "Price"* of the product and 
   click on *"Add product"*
5. Now you can enter further data as you would do for standard products. Please
   note that this are in this case just default values, which can be 
   overwritten for every single variant later
6. Change the product's type from *"Standard"* to *"Product with variants"* 
   and click *"change"*. You will notice that there is now a *"Variants"* tab

Go to the *"Variants"* tab

7. Enter *"Color"* in the *"Local properties"* field and click on
   *"Add property"*. (Please note: you can also use properties and property
   groups to create variants, but this is beyond that how-to).
8. Enter *"Red"* into the now provided field and click on 
   *"Add option"* 
9. Repeat that with *"Green" and "Blue"*. Please note: for convenience you can 
   also add *"Red, Green, Blue"* to add all options at once
10. You should now see the Property *"Color"* with three options *"Red"*,
    *"Green"* and *"Blue"*
11. Now go to *"Variants"*, select *"all"* and click on "*Add variants(s)"*. 
    You should now see the automatically added variants

Click on the pencil of a variant to in order to open its edit form
    
12. Activate (select the checkbox beside the label of field) and fill in all
    fields which differ from the parent product. If you don't select the
    checkbox the field value will be inherited from the parent product
13. Repeat that for every variant you want to change

Go back to the parent product (you can click on the *"Parent"* link) and 
select the *"Variants"* tab.

14. Select the *"Variants display type"*:

    * *"List"*: All variants will be displayed within a list of radio buttons
    * *"Select"*: Variants can be selected with select boxes.    
    
15. Activate all variants you want to display to the shop customers

Go to the *"Product"* tab.

16. Select the "Active" checkbox

17. Click on *"View product"* and you will see your newly created *"Product
    with variants"*
