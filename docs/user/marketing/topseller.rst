.. index:: Topseller

==========
Top seller
==========

Overview
=========

* Top sellers are displayed within the top seller portlet.
* Top sellers are calculated automatically by default
* Top sellers can be added manually. These have precedence over automatically
  ones.

Navigation / Filtering
----------------------

You can filter the available products within the ``Products`` section:

.. image:: /images/topseller_navigation.*

* Use the navigation link to navigate through the displayed products.
* To filter the products by name fill in the text box.
* To filter the products by category select the category from the select box.

Add top sellers
---------------

To add top sellers check the checkboxes beside the products you want to add
(within the ``Products`` section) and click on ``Add to topseller``.

.. image:: /images/topseller_products.*

Update top sellers
------------------

To update top sellers change the entries in question and click on the ``Save
topseller`` button (see image below).

Remove top sellers
------------------

To remove top sellers check the checkboxes beside the products you want to
remove (within the ``Topseller`` section) and click on ``Remove from
topseller``.

.. image:: /images/topseller_topseller.*