.. index:: Voucher, Voucher Group

========
Vouchers
========

Overview
=========

* Vouchers are grouped in voucher groups.
* Vouchers can be absolute or percentage.
* Vouchers can have a start and a end date.
* Vouchers can have a minimum cart price to be active.
* Vouchers can have flexible voucher numbers.

Add a voucher group
===================

.. image:: /images/vouchers_actions.*

To add a voucher group proceed as following:

1. Click on the ``Add voucher group`` button.
2. Fill in a ``name`` and a ``position``.
3. Click on "Add voucher group" button.

Delete a voucher group
======================

To delete the current voucher group proceed as following:

1. Click on the ``Delete group`` button
2. Answer the confirmation question with ``yes``.

Tabs
====

This section describes the single tabs of a voucher group.

Data
====

Within the data tab you can change the name of the voucher group.

.. image:: /images/vouchers_data.*

Name
    The name of the voucher group (this is just for internal usage).
Position
    Voucher groups are ordered by position, lower number comes first.

Vouchers
========

Within the vouchers tab you can add and delete the vouchers of the current
voucher group.

**Add Vouchers**

.. image:: /images/vouchers_add.*

To add vouchers to a vouchers group proceed as following:

1. Click on the ``Vouchers`` tab
2. Fill in the ``Add vouchers`` form
3. Click on the ``Add vouchers`` button

Amount
    The amount of voucher which are supposed to be created.

Value
    The value of the voucher. This is either an absolute or an percentage
    value dependent on the type of the voucher (see below).

Start/End:
    The range when the vouchers are valid.

Kind of:
    The type of the voucher. Either absolute or percentage.

Effective from:
    The minimum cart price from which the voucher are valid. This is only the
    total cart item prices without shipping and payment costs.

Tax
    The tax of the vouchers. If you don't know the tax just let it empty.
    
**Delete vouchers**

To delete vouchers check the checkbox beside the vouchers you want to delete
and click on the ``Delete vouchers`` button.

.. image:: /images/vouchers_vouchers.*

Options
=======

Within the options tab you can change the options for voucher numbers.

.. image:: /images/vouchers_options.*

Number prefix
    The prefix of the voucher number: PREFIX-xxxxx.

Number suffix
    The suffix of the voucher number: xxxxx-SUFFIX.

Number length
    The length of the voucher number.

Number letters
    The used letters to create a voucher number

.. note::

    The current options are only effective for upcoming vouchers.

