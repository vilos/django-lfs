.. index:: Property

==========
Properties
==========

Overview
--------

* Properties are used to add flexible properties on a product, like color, 
  size, material, etc.
* There are two types of properties: global properties and local properties:

Global properties
-----------------

* Global properites are organized with property groups (which are assigned to 
  products).
* Global properites can be used to create product filters.
* Global properties can be used to create variants.

.. note:: 

    To create filters you have to use global properties.

Local properties
-----------------

* Local properites are organized on a single product.
* Local properties can be used to create variants.

More information 
----------------

* :ref:`Management reference <product-properties-label>`
* :doc:`/user/howtos/how_to_filters`
* :doc:`/user/howtos/how_to_variants`