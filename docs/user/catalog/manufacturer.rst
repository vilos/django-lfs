============
Manufacturer
============

.. image:: /images/manufacturer_overview.*

Overview
========

* The manufacturer is the producer of the product.
* A product can have at most one manufacturer.

Add a manufacturer
==================

To add a manufacturer proceed as following:

1. Click on the ``Add Manufacturer`` button. (If there is no manufacturer at all
   yet you will be redirected automatically to the add form).
2. Enter the name of the manufacturer.
3. Click on the ``Save`` button.

Remove a manufacturer
=====================

To remove a manufacturer proceed as following:

1. Select the manufacturer you want to delete.
2. Click on the ``Delete Manufacturer`` button.
3. Answer the confirmation question with ``yes``.

Assign products
================

To assign products to the manufacturer proceed as following:

1. Select the manufacturer in question.
2. Go to the ``Product selection`` tab.
3. Select all checkboxes beside the products you want to assign. You can also add
   whole categories by clicking on a category knot within the tree (see the image
   above).

Remove products
===============

To remove products to the manufacturer proceed as following:

1. Select the manufacturer in question.
2. Go to the ``Product selection`` tab.
3. Deselect the checkboxes beside the product/category you want to remove.
