=============
Miscellaneous
=============

Overview
========

Here are some miscellaneous utils for LFS. Actually you should never have to 
use theme.

Clear Cache
===========

LFS provides an aggresive caching strategy for all content like categories and 
products (dependen on your environment). Actually LFS deletes outdating content
automatically. However if you experience old content you can always empty the 
cache automatically.

Set Category Levels
===================

LFS manages internal category levels. Here you can create them safely new if 
something went wrong. Howwever, you should never have to use this.