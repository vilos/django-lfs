Glossary
========

.. glossary::

    Portlet
        A portlet is a piece of content which are managed in :term:`Slots`.

    Portlets
        see :term:`Portlet`

    Slot
        Slots can be freely defined and placed anywhere within a HTML page. LFC
        has by default two slots: a left and a right slot beside the content in
        the middle of the page. For more information see `django-portlets
        <http://packages.python.org/django-portlets/>`_

    Slots
        see :term:`Slot`
    
    Static Block
        A static block is a piece of HTML which can be reused by several content
        objects like the shop, a category or a product.
        
    Templates
        Templates are used to structure the content (text, image, files, sub pages)
        of a page. There are just a view simple templates by default. 3rd-party
        developers can add more templates.

    WYSIWYG
        What You See Is What You Get