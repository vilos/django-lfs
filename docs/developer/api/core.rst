====
Core
====

Utilities
=========

.. warning::

    LFS is in alpha state. Please consider the API as supposed to be changed
    until it reaches beta state.

Classes
-------

.. autoclass:: lfs.core.utils.CategoryTree
    :members:

.. autoclass:: lfs.core.utils.LazyEncoder
    :members:

Functions
---------

Miscellaneous
^^^^^^^^^^^^^

.. autofunction:: lfs.core.utils.get_default_shop

.. autofunction:: lfs.core.utils.lfs_quote

.. autofunction:: lfs.core.utils.import_module

.. autofunction:: lfs.core.utils.get_current_categories

.. autofunction:: lfs.core.utils.set_category_levels

.. autofunction:: lfs.core.utils.getLOL

.. autofunction:: lfs.core.utils.render_to_ajax_response

Date
^^^^

.. autofunction:: lfs.core.utils.get_start_day

.. autofunction:: lfs.core.utils.get_end_day

Redirect
^^^^^^^^

.. autofunction:: lfs.core.utils.get_redirect_for

.. autofunction:: lfs.core.utils.set_redirect_for

.. autofunction:: lfs.core.utils.remove_redirect_for
