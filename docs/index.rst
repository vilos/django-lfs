LFS - Lightning Fast Shop
=========================

LFS is a online-shop based on Django.

Introduction
============

.. toctree::
   :maxdepth: 2

   introduction/overview.rst
   introduction/installation.rst
   introduction/getting_started.rst

Users
=====

Management reference
--------------------

.. toctree::
   :maxdepth: 2

   user/catalog/index.rst
   user/properties/index.rst
   user/html/index.rst
   user/customer/index.rst
   user/marketing/index.rst
   user/utils/index.rst

Miscellaneous information
-------------------------

.. toctree::
   :maxdepth: 2

   user/misc/criteria.rst
   user/misc/properties.rst

How-tos
-------

.. toctree::
   :maxdepth: 1

   user/howtos/how_to_horizontal_menu.rst
   user/howtos/how_to_filters.rst
   user/howtos/how_to_variants.rst
   user/howtos/how_to_paypal.rst
   user/howtos/how_to_shipping_method
   user/howtos/how_to_payment_method

Developers
==========

API
---

.. toctree::
   :maxdepth: 2

   developer/api/core

How-tos
-------

.. toctree::
   :numbered:
   :maxdepth: 1

   developer/howtos/how_to_add_own_templates.rst
   developer/howtos/how_to_add_own_payment_methods.rst
   developer/howtos/how_to_export_product_script
   
Indices and tables
==================

* :doc:`glossary`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

